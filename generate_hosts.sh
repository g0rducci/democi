#!/bin/bash

# Recopilar las direcciones IP de las instancias privadas de EC2
private_ip_1=$(terraform output -raw private_ip_1)
private_ip_2=$(terraform output -raw private_ip_2)
# Agregar más instancias privadas si es necesario

# Crear el archivo hosts de Ansible
cat <<EOL > hosts
[servers]
${private_ip_1} ansible_user=ubuntu ansible_ssh_private_key_file=keyforbastion
${private_ip_2} ansible_user=ubuntu vansible_ssh_private_key_file=keyforbastion
[all:vars]
ansible_python_interpreter=/usr/bin/python3
# Agregar más instancias privadas si es necesario
EOL
